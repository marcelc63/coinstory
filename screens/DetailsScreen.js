import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  TouchableOpacity,
  Text,
  Image,
  ScrollView,
  TextInput,
  Button
} from "react-native";
import moment from "moment";

Number.prototype.formatMoney = function(c, d, t) {
  var n = this,
    c = isNaN((c = Math.abs(c))) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt((n = Math.abs(Number(n) || 0).toFixed(c)))),
    j = (j = i.length) > 3 ? j % 3 : 0;
  return (
    s +
    (j ? i.substr(0, j) + t : "") +
    i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
    (c
      ? d +
        Math.abs(n - i)
          .toFixed(c)
          .slice(2)
      : "")
  );
};

export default class DetailsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    return {
      title: params ? params.name : "Coin Details"
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      store: {},
      lastUpdate: new Date().getTime(),
      idr: 0,
      usd: 0,
      coin: 1
    };
  }

  componentWillMount() {
    this.getData();
  }

  getData() {
    const { params } = this.props.navigation.state;

    return fetch(
      "https://api.coinmarketcap.com/v1/ticker/" + params.id + "/?convert=IDR"
    )
      .then(response => response.json())
      .then(responseJson => {
        let store = responseJson[0];
        this.setState({
          store: store,
          idr: store.price_idr,
          usd: store.price_usd,
          lastUpdate: new Date().getTime()
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  calculate(payload) {
    const { store } = this.state;
    console.log(payload.value);

    if (
      payload.value === 0 ||
      payload.value === null ||
      payload.value === undefined ||
      payload.value === ""
    ) {
      this.setState({ idr: 0, usd: 0, coin: 0 });
    } else {
      payload.value = parseFloat(payload.value.split(",").join(""));
      if (payload.type === "idr") {
        let idr = payload.value;
        let usd = payload.value / (store.price_idr / store.price_usd);
        let coin = payload.value / store.price_idr;
        this.setState({ idr: idr, usd: usd, coin: coin });
      } else if (payload.type === "usd") {
        let idr = payload.value / (store.price_usd / store.price_idr);
        let usd = payload.value;
        let coin = payload.value / store.price_usd;
        this.setState({ idr: idr, usd: usd, coin: coin });
      } else if (payload.type === "coin") {
        let idr = payload.value * store.price_idr;
        let usd = payload.value * store.price_usd;
        let coin = payload.value;
        this.setState({ idr: idr, usd: usd, coin: coin });
      }
    }
  }

  render() {
    const { params } = this.props.navigation.state;
    const { store } = this.state;

    let image =
      "https://s3-us-west-1.amazonaws.com/coin-tracker-public/crypto-icons/64x64/" +
      store.symbol +
      ".png";
    image = image.toLowerCase();

    let change_1h = Number(store["percent_change_1h"]).toFixed(2);
    let changeStyle_1h = styles.changeNegative;
    if (change_1h > 0) {
      changeStyle_1h = styles.changePositive;
      change_1h = "+" + change_1h;
    }

    let change_24h = Number(store["percent_change_24h"]).toFixed(2);
    let changeStyle_24h = styles.changeNegative;
    if (change_24h > 0) {
      changeStyle_24h = styles.changePositive;
      change_24h = "+" + change_24h;
    }

    let change_7d = Number(store["percent_change_7d"]).toFixed(2);
    let changeStyle_7d = styles.changeNegative;
    if (change_7d > 0) {
      changeStyle_7d = styles.changePositive;
      change_7d = "+" + change_7d;
    }

    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.row}>
            <View style={styles.floatLeft}>
              <Image style={styles.image} source={{ uri: image }} />
              <View style={styles.info}>
                <Text style={styles.name}>{params.name}</Text>
                <Text style={styles.label}>
                  Last Updated{" "}
                  {moment(store.last_updated * 1000).format("h:mm A")}
                </Text>
              </View>
            </View>
          </View>

          <Text style={styles.section}>Price</Text>
          <View style={styles.row}>
            <View style={styles.floatCenter}>
              <Text style={styles.value}>
                {Number(store.price_usd).toLocaleString("en", {
                  maximumFractionDigits: 4
                })}
              </Text>
              <Text style={styles.label}>USD</Text>
            </View>
            <View style={styles.floatCenter}>
              <Text style={styles.value}>
                {Number(store.price_idr).formatMoney(0)}
              </Text>
              <Text style={styles.label}>IDR</Text>
            </View>
            <View style={styles.floatCenter}>
              <Text style={styles.value}>
                {Number(store.price_btc).toLocaleString("en", {
                  maximumFractionDigits: 4
                })}
              </Text>
              <Text style={styles.label}>BTC</Text>
            </View>
          </View>

          <Text style={styles.section}>Price Calculator</Text>
          <View style={styles.calculator}>
            <View style={styles.row}>
              <Text style={styles.inputLabel}>IDR</Text>
              <TextInput
                style={styles.textInput}
                keyboardType="numeric"
                onChangeText={value =>
                  this.calculate({ value: value, type: "idr" })
                }
                editable={true}
                value={String(
                  Number(this.state.idr).toLocaleString("en", {
                    maximumFractionDigits: 10
                  })
                )}
              />
            </View>
            <View style={styles.row}>
              <Text style={styles.inputLabel}>USD</Text>
              <TextInput
                style={styles.textInput}
                keyboardType="numeric"
                onChangeText={value =>
                  this.calculate({ value: value, type: "usd" })
                }
                editable={true}
                value={String(
                  Number(this.state.usd).toLocaleString("en", {
                    maximumFractionDigits: 10
                  })
                )}
              />
            </View>
            <View style={styles.row}>
              <Text style={styles.inputLabel}>{store.symbol}</Text>
              <TextInput
                style={styles.textInput}
                keyboardType="numeric"
                onChangeText={value =>
                  this.calculate({ value: value, type: "coin" })
                }
                editable={true}
                value={String(
                  Number(this.state.coin).toLocaleString("en", {
                    maximumFractionDigits: 10
                  })
                )}
              />
            </View>
            <TouchableOpacity onPress={() => this.calculate({ value: 0 })}>
              <Text style={styles.resetCalculator}>Reset Calculator</Text>
            </TouchableOpacity>
          </View>

          <Text style={styles.section}>Percent Change</Text>
          <View style={styles.row}>
            <View style={styles.floatCenter}>
              <Text style={changeStyle_1h}>{change_1h}%</Text>
              <Text style={styles.label}>1h</Text>
            </View>
            <View style={styles.floatCenter}>
              <Text style={changeStyle_24h}>{change_24h}%</Text>
              <Text style={styles.label}>24h</Text>
            </View>
            <View style={styles.floatCenter}>
              <Text style={changeStyle_7d}>{change_7d}%</Text>
              <Text style={styles.label}>7d</Text>
            </View>
          </View>

          <Text style={styles.section}>Volume</Text>
          <View style={styles.row}>
            <View style={styles.floatCenter}>
              <Text style={styles.value}>
                {Number(store["24h_volume_usd"]).formatMoney(0)}
              </Text>
              <Text style={styles.label}>24h - USD</Text>
            </View>
            <View style={styles.floatCenter}>
              <Text style={styles.value}>
                {Number(store["24h_volume_idr"]).formatMoney(0)}
              </Text>
              <Text style={styles.label}>24h - IDR</Text>
            </View>
          </View>

          <Text style={styles.section}>Market Cap</Text>
          <View style={styles.row}>
            <View style={styles.floatCenter}>
              <Text style={styles.value}>
                {Number(store.market_cap_usd).formatMoney(0)}
              </Text>
              <Text style={styles.label}>USD</Text>
            </View>
            <View style={styles.floatCenter}>
              <Text style={styles.value}>
                {Number(store.market_cap_idr).formatMoney(0)}
              </Text>
              <Text style={styles.label}>IDR</Text>
            </View>
          </View>

          <Text style={styles.section}>Supply</Text>
          <View style={styles.row}>
            <View style={styles.floatCenter}>
              <Text style={styles.value}>
                {Number(store.available_supply).formatMoney(0)}
              </Text>
              <Text style={styles.label}>Available - {store.symbol}</Text>
            </View>
            <View style={styles.floatCenter}>
              <Text style={styles.value}>
                {Number(store.total_supply).formatMoney(0)}
              </Text>
              <Text style={styles.label}>Total - {store.symbol}</Text>
            </View>
            <View style={styles.floatCenter}>
              <Text style={styles.value}>
                {Number(store.max_supply).formatMoney(0)}
              </Text>
              <Text style={styles.label}>Max - {store.symbol}</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff",
    justifyContent: "flex-start"
  },
  calculator: {
    flex: 1,
    flexDirection: "column",
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    backgroundColor: "#f1f1f1",
    marginTop: 10,
    marginBottom: 5
  },
  calculatorTitle: {
    color: "#222",
    paddingTop: 10,
    paddingLeft: 15,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center"
  },
  resetCalculator: {
    color: "#fff",
    backgroundColor: "#00a1ff",
    textAlign: "center",
    borderRadius: 15,
    padding: 5,
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10
  },
  textInput: {
    flexGrow: 1,
    width: 100,
    borderColor: "#f1f1f1",
    borderWidth: 1,
    fontSize: 16,
    textAlign: "left",
    fontWeight: "bold"
  },
  inputLabel: {
    color: "#999",
    fontSize: 17,
    paddingRight: 10,
    paddingLeft: 5,
    width: 50
  },
  image: {
    height: 40,
    width: 40
  },
  row: {
    flex: 1,
    flexDirection: "row",
    padding: 10,
    justifyContent: "space-between"
  },
  info: {
    paddingLeft: 10
  },
  floatLeft: {
    flex: 1,
    flexDirection: "row"
  },
  floatRight: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  floatCenter: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 5,
    backgroundColor: "#f1f1f1"
  },
  section: {
    color: "#222",
    paddingTop: 10,
    paddingLeft: 15,
    fontSize: 16,
    fontWeight: "bold"
  },
  label: {
    color: "#999",
    fontSize: 12
  },
  value: {
    fontWeight: "bold",
    marginBottom: 5
  },
  name: {
    fontWeight: "bold",
    fontSize: 16,
    marginBottom: 5
  },
  changePositive: {
    fontWeight: "bold",
    marginBottom: 5,
    color: "#00d607"
  },
  changeNegative: {
    fontWeight: "bold",
    marginBottom: 5,
    color: "#d60000"
  }
});
