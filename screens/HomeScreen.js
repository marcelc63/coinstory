import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity
} from "react-native";
import { WebBrowser, LinearGradient } from "expo";
import { EventRegister } from "react-native-event-listeners";

import NewsFeed from "../components/NewsFeed.js";
import { MonoText } from "../components/StyledText";

const WINDOW_HEIGHT = Dimensions.get("window").height;
const WINDOW_WIDTH = Dimensions.get("window").width;

class LogoTitle extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={() => EventRegister.emit("scrollToTop")}>
        <Text style={styles.header}>Coinstory</Text>
      </TouchableOpacity>
    );
  }
}

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />
  };

  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    this.componentDidFocus();
  }

  componentDidFocus = () => {
    this.props.navigation.addListener("didFocus", payload => {
      EventRegister.emit("homeScreenDidFocus");
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <NewsFeed />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    fontWeight: "bold",
    fontSize: 17,
    padding: 10
  }
});
