import React from "react";
import {
  StyleSheet,
  Text,
  SectionList,
  View,
  Image,
  AppState,
  TouchableOpacity,
  TouchableWithoutFeedback
} from "react-native";
import moment from "moment";
import { EventRegister } from "react-native-event-listeners";

class LogoTitle extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={() => EventRegister.emit("scrollToTop")}>
        <Text style={styles.header}>Coinwatch</Text>
      </TouchableOpacity>
    );
  }
}

Number.prototype.formatMoney = function(c, d, t) {
  var n = this,
    c = isNaN((c = Math.abs(c))) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt((n = Math.abs(Number(n) || 0).toFixed(c)))),
    j = (j = i.length) > 3 ? j % 3 : 0;
  return (
    s +
    (j ? i.substr(0, j) + t : "") +
    i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
    (c
      ? d +
        Math.abs(n - i)
          .toFixed(c)
          .slice(2)
      : "")
  );
};

export default class AnalyticsScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />
  };

  constructor(props) {
    super(props);

    this.state = {
      marketStore: [],
      refreshing: false,
      lastUpdate: new Date().getTime(),
      appState: AppState.currentState
    };
  }

  componentWillMount() {
    this.getData();
    this._eventListener();
    this.componentDidFocus();
  }

  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  componentDidFocus = () => {
    this.props.navigation.addListener("didFocus", payload => {
      this.getData();
    });
  };

  getData() {
    console.log("fetch marketcap");
    return fetch(
      "https://api.coinmarketcap.com/v1/ticker/?convert=IDR&limit=100"
    )
      .then(response => response.json())
      .then(responseJson => {
        let store = responseJson;
        this.setState({ marketStore: store, lastUpdate: new Date().getTime() });
      })
      .catch(error => {
        console.error(error);
      });
  }

  _eventListener() {
    this.listener = EventRegister.addEventListener("scrollToTop", data => {
      this.refs._scrollView.scrollToLocation({ itemIndex: 0 });
    });
  }

  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      console.log("App has come to the foreground!");
      this.getData();
    }
    this.setState({ appState: nextAppState });
  };

  _onRefresh() {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.getData();
      this.setState({ refreshing: false });
    }, 2000);
  }

  _openDetails(payload) {
    const { navigate } = this.props.navigation;
    navigate("Details", { name: payload.name, id: payload.id });
  }

  _renderItem(item) {
    if (item.type === undefined) {
      let image =
        "https://s3-us-west-1.amazonaws.com/coin-tracker-public/crypto-icons/64x64/" +
        item.symbol +
        ".png";
      image = image.toLowerCase();
      let change = Number(item["percent_change_1h"]).toFixed(2);
      let changeStyle = styles.changeNegative;
      if (change > 0) {
        changeStyle = styles.changePositive;
        change = "+" + change;
      }

      return (
        <TouchableOpacity
          onPress={() => this._openDetails({ name: item.name, id: item.id })}
        >
          <View style={styles.row}>
            <View style={styles.float}>
              <Image style={styles.image} source={{ uri: image }} />
              <Text style={styles.text}>{item.name}</Text>
            </View>
            <View style={styles.floatRight}>
              <Text style={styles.price}>
                $ {Number(item.price_usd).formatMoney(4)}
              </Text>
              <Text style={changeStyle}>{change}%</Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.row}>
          <View style={styles.float}>
            <Text style={styles.bold}>Top 100 Coins</Text>
          </View>
          <Text style={styles.price}>
            Last Update {moment(this.state.lastUpdate).format("h:mm A")}
          </Text>
        </View>
      );
    }
  }

  render() {
    return (
      <SectionList
        ref="_scrollView"
        style={styles.container}
        sections={[
          { data: [{ type: "title" }] },
          { data: [...this.state.marketStore] }
        ]}
        renderItem={({ item }) => this._renderItem(item)}
        keyExtractor={item => item.rank}
        refreshing={this.state.refreshing}
        onRefresh={this._onRefresh.bind(this)}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff"
  },
  row: {
    flex: 1,
    flexDirection: "row",
    padding: 10,
    justifyContent: "space-between"
  },
  float: {
    flex: 1,
    flexDirection: "row"
  },
  floatRight: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  image: {
    height: 16,
    width: 16
  },
  text: {
    alignSelf: "flex-start",
    paddingLeft: 5,
    fontWeight: "500"
  },
  bold: {
    fontWeight: "bold"
  },
  price: {
    alignSelf: "flex-end",
    textAlign: "right",
    color: "#888",
    marginRight: 5
  },
  header: {
    fontWeight: "bold",
    fontSize: 17,
    padding: 10
  },
  changePositive: {
    color: "#00d607"
  },
  changeNegative: {
    color: "#d60000"
  }
});
