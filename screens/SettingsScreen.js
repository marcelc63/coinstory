import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  TouchableOpacity,
  Text,
  AsyncStorage
} from "react-native";

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: "Options"
  };

  constructor(props) {
    super(props);

    this.state = {
      lang: "EN"
    };
  }

  componentWillMount() {
    this.getLang();
  }

  getLang() {
    AsyncStorage.getItem("lang", (e, res) => {
      if (res !== null) {
        this.setState({ lang: res });
      }
    });
  }

  setLang(lang) {
    AsyncStorage.setItem("lang", lang, (e, res) => {
      this.setState({ lang: lang });
    });
  }

  clearStorage() {
    AsyncStorage.clear();
  }

  langButton() {
    if (this.state.lang === "EN") {
      return (
        <TouchableOpacity
          onPress={() => {
            this.setLang("ID");
          }}
          style={styles.button}
        >
          <Text>Switch to Indonesian</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => {
            this.setLang("EN");
          }}
          style={styles.button}
        >
          <Text>Switch to English</Text>
        </TouchableOpacity>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Language</Text>
        {this.langButton()}
        <TouchableOpacity
          onPress={() => {
            this.clearStorage();
          }}
          style={styles.button}
        >
          <Text>Clear Settings</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff"
  },
  title: {
    fontWeight: "600",
    padding: 10
  },
  button: {
    flexDirection: "column",
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    borderBottomColor: "#ddd",
    padding: 10
  }
});
