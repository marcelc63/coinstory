import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  AsyncStorage,
  Button,
  Platform
} from "react-native";
import { WebBrowser, LinearGradient } from "expo";

import Onboarding from "react-native-onboarding-swiper";

import { MonoText } from "../components/StyledText";

const WINDOW_HEIGHT = Dimensions.get("window").height;
const WINDOW_WIDTH = Dimensions.get("window").width;

export default class OnboardingScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  chooseLanguage() {
    return (
      <View>
        <Button title="Switch to Indonesia" onPress={() => {}} />
      </View>
    );
  }

  _complete() {
    AsyncStorage.setItem("onboard", "onboarded", (e, res) => {
      this.props.screenProps();
    });
  }

  render() {
    let screen2 =
      Platform.OS === "ios"
        ? require("../assets/onboarding/screen-2.gif")
        : require("../assets/onboarding/screen-2.png");
    let screen3 =
      Platform.OS === "ios"
        ? require("../assets/onboarding/screen-3.gif")
        : require("../assets/onboarding/screen-3.png");
    return (
      <Onboarding
        pages={[
          {
            backgroundColor: "#fff",
            image: (
              <Image
                source={require("../assets/onboarding/screen-1.png")}
                style={styles.screen1}
              />
            ),
            title: "Stay Updated",
            subtitle:
              "Coinstory serves you bite size crypto news in carousell and story format."
          },
          {
            backgroundColor: "#fff",
            image: <Image source={screen2} style={styles.screen2} />,
            title: "Double Tap or Swipe Left",
            subtitle: "Summaries on News Feed. Read what matters quickly."
          },
          {
            backgroundColor: "#fff",
            image: <Image source={screen3} style={styles.screen3} />,
            title: "Swipe Up",
            subtitle: "Dive deep. Know the full story."
          },
          {
            backgroundColor: "#fff",
            image: (
              <Image
                source={require("../assets/onboarding/screen-4.png")}
                style={styles.screen4}
              />
            ),
            title: "Available in Bahasa Indonesia",
            subtitle: "Switch to any language anytime."
          },
          {
            backgroundColor: "#fff",
            image: (
              <Image
                source={require("../assets/images/icon.png")}
                style={styles.screen5}
              />
            ),
            title: "Have fun!",
            subtitle: "Enjoy :)"
          }
        ]}
        onDone={() => {
          this._complete();
        }}
        onSkip={() => {
          this._complete();
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  screen1: {
    width: WINDOW_WIDTH * 0.5,
    height: WINDOW_HEIGHT * 0.5,
    borderRadius: 10
  },
  screen2: {
    width: WINDOW_WIDTH * 0.6,
    height: WINDOW_WIDTH * 0.6,
    borderRadius: 10
  },
  screen3: {
    width: WINDOW_WIDTH * 0.5,
    height: WINDOW_HEIGHT * 0.5,
    borderRadius: 10
  },
  screen4: {
    width: WINDOW_WIDTH * 0.5,
    height: WINDOW_WIDTH * 0.5,
    borderRadius: 10
  },
  screen5: {
    width: WINDOW_WIDTH * 0.5,
    height: WINDOW_WIDTH * 0.5,
    borderRadius: 10
  }
});
