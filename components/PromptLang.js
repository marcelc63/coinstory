import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Modal,
  Button,
  Dimensions,
  AsyncStorage
} from "react-native";

const WINDOW_HEIGHT = Dimensions.get("window").height;
const WINDOW_WIDTH = Dimensions.get("window").width;

export default class PromptLang extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: true
    };
  }

  componentWillMount() {
    AsyncStorage.getItem("lang", (e, res) => {
      if (res !== null) {
        this.setState({ visible: false });
      }
    });
  }

  _chooseLanguage(lang) {
    AsyncStorage.setItem("lang", lang, (e, res) => {
      this.setState({ visible: false });
    });
    this.props.handlePress();
  }

  render() {
    return (
      <Modal
        visible={this.state.visible}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.container}>
          <View style={styles.modal}>
            <Text style={styles.prompt}>
              Choose your language. You can change it later in the Settings.
            </Text>
            <Button
              title="English"
              onPress={() => this._chooseLanguage("EN")}
              style={styles.choice}
            />
            <Button
              title="Indonesian"
              onPress={() => this._chooseLanguage("ID")}
              style={styles.choice}
            />
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width: WINDOW_WIDTH * 0.6,
    height: WINDOW_HEIGHT * 0.35,
    backgroundColor: "#fff",
    borderRadius: 10,
    padding: 10,
    shadowColor: "#222",
    shadowOffset: {
      width: -1,
      height: 1
    },
    shadowRadius: 10,
    shadowOpacity: 0.9,
    justifyContent: "center"
  },
  prompt: {
    alignSelf: "center",
    textAlign: "center",
    marginBottom: 30
  },
  choice: {
    alignSelf: "center",
    textAlign: "center",
    color: "#009cff",
    fontSize: 18,
    marginBottom: 20
  }
});
