import React from "react";
import { View, TouchableWithoutFeedback } from "react-native";

export default class TouchDouble extends React.Component {
  onDoublePress = callback => {
    const time = new Date().getTime();
    const delta = time - this.lastPress;
    console.log("test");
    const DOUBLE_PRESS_DELAY = 400;
    if (delta < DOUBLE_PRESS_DELAY) {
      console.log("double");
      callback();
    }
    this.lastPress = time;
  };

  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.onDoublePress(this.props.handlePress);
        }}
      >
        <View style={this.props.style}>{this.props.children}</View>
      </TouchableWithoutFeedback>
    );
  }
}
