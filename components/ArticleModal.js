import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  PanResponder,
  Animated,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Modal,
  Button,
  Easing,
  Dimensions,
  ScrollView
} from "react-native";
import { EvilIcons } from "@expo/vector-icons";

const WINDOW_HEIGHT = Dimensions.get("window").height;
const WINDOW_WIDTH = Dimensions.get("window").width;
const SWIPE_THRESHOLD_FIRST = WINDOW_WIDTH * 0.066;
const SWIPE_THRESHOLD_GRANT_LEFT = WINDOW_WIDTH * 0.111;
const SWIPE_THRESHOLD_GRANT_RIGHT = WINDOW_WIDTH * 0.888;
const SWIPE_THRESHOLD_MOVE = WINDOW_WIDTH * 0.1066;

export default class ArticleModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pan: new Animated.ValueXY(),
      scale: new Animated.Value(1),
      opacity: new Animated.Value(1),
      scrollEnabled: true,
      paragraphStore: []
    };
  }

  componentWillMount() {
    this._mounted = true;

    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponderCapture: () => true,
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderGrant: (e, gestureState) => {
        console.log(gestureState);
        this.state.pan.setOffset({
          x: this.state.pan.x._value,
          y: this.state.pan.y._value
        });
        this.state.pan.setValue({ x: 0, y: 0 });
        // if(Math.abs(gestureState.dx) >= SWIPE_THRESHOLD_FIRST){
        //   this.setState({scrollEnabled: false})
        // }
        if (
          gestureState.x0 >= SWIPE_THRESHOLD_GRANT_RIGHT ||
          gestureState.x0 <= SWIPE_THRESHOLD_GRANT_LEFT
        ) {
          this.setState({ scrollEnabled: false });
        } else {
          this.setState({ scrollEnabled: true });
        }
      },
      onPanResponderMove: (e, gestureState) => {
        if (
          gestureState.x0 >= SWIPE_THRESHOLD_GRANT_RIGHT ||
          gestureState.x0 <= SWIPE_THRESHOLD_GRANT_LEFT
        ) {
          console.log("onMove");
          if (Math.abs(gestureState.dx) >= SWIPE_THRESHOLD_FIRST) {
            Animated.event([
              null,
              {
                dx: this.state.pan.x
              }
            ])(e, gestureState);
            this.setState({ scrollEnabled: false });
          }
        } else {
          this.setState({ scrollEnabled: true });
        }
      },
      onPanResponderEnd: (e, gestureState) => {
        console.log("onEnd");
      },
      onPanResponderRelease: (e, gestureState) => {
        console.log("onRelease");
        this.state.pan.flattenOffset();
        if (
          Math.abs(gestureState.dx) >= SWIPE_THRESHOLD_MOVE &&
          this._mounted
        ) {
          let endValue = 0;
          if (gestureState.dx > 0) {
            endValue = WINDOW_WIDTH;
          } else {
            endValue = -1 * WINDOW_WIDTH;
          }

          Animated.timing(this.state.pan.x, {
            toValue: endValue,
            duration: 100,
            easing: Easing.linear()
          }).start(() => {
            setTimeout(() => {
              this.closeSelf();
            }, 200);
          });
        } else {
          Animated.timing(this.state.pan.x, {
            toValue: 0,
            friction: 3,
            duration: 100
          }).start(() => {
            this.setState({ scrollEnabled: true });
          });
        }
      },
      onPanResponderTerminate: (e, gestureState) => {
        console.log("onTerminate");
        this.state.pan.flattenOffset();

        if (
          Math.abs(gestureState.dx) >= SWIPE_THRESHOLD_MOVE &&
          this._mounted
        ) {
          let endValue = 0;
          if (gestureState.dx > 0) {
            endValue = WINDOW_WIDTH;
          } else {
            endValue = -1 * WINDOW_WIDTH;
          }

          Animated.timing(this.state.pan.x, {
            toValue: endValue,
            duration: 100,
            easing: Easing.linear()
          }).start(() => {
            setTimeout(() => {
              this.closeSelf();
            }, 200);
          });
        }
      }
    });

    this.getParagraph();
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  getParagraph() {
    console.log("fetch");
    return fetch(
      "https://coinstoryapp.com/getParagraph?uniqid=" +
        this.props.payload.uniqid
    )
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.meta.code === 200) {
          let store = responseJson.data;
          this.setState({ paragraphStore: store });
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  closeSelf() {
    console.log("Article Gone");
    this.props.closeArticle();
  }

  render() {
    let paragraph = this.state.paragraphStore.map((item, index) => (
      <Text style={styles.sentence} key={index}>
        {item.text}
      </Text>
    ));

    let { pan, scale } = this.state;

    let newY = pan.x.interpolate({
      inputRange: [-1 * WINDOW_WIDTH, 0, WINDOW_WIDTH],
      outputRange: [WINDOW_WIDTH, 0, WINDOW_WIDTH]
    });
    let [translateX, translateY] = [pan.x, newY];
    let rotate = pan.x.interpolate({
      inputRange: [-1 * WINDOW_WIDTH, 0, WINDOW_WIDTH],
      outputRange: ["-30deg", "0deg", "30deg"]
    });
    // let rotate = '0deg'
    let backgroundOpacity = pan.x.interpolate({
      inputRange: [-1 * WINDOW_WIDTH, 0, WINDOW_WIDTH],
      outputRange: [
        "rgba(0, 0, 0, 0)",
        "rgba(0, 0, 0, 0.9)",
        "rgba(0, 0, 0, 0)"
      ]
    });
    let backgroundModal = {
      backgroundColor: backgroundOpacity
    };
    let imageStyle = {
      transform: [{ translateX }, { translateY }, { rotate }, { scale }]
    };

    return (
      <Modal
        visible={this.props.articleVisible}
        animationType={"slide"}
        transparent={true}
        onRequestClose={() => {}}
      >
        <Animated.View style={backgroundModal}>
          <Animated.View style={imageStyle}>
            <ScrollView
              style={styles.scrollView}
              scrollEnabled={this.state.scrollEnabled}
              scrollEventThrottle={100}
              onScroll={event => {
                if (event.nativeEvent.contentOffset.y > 0) {
                  this._scrollEvent = true;
                } else if (event.nativeEvent.contentOffset.y === 0) {
                  this._scrollEvent = false;
                } else if (
                  event.nativeEvent.contentOffset.y < 0 &&
                  !this._scrollEvent &&
                  this.state.scrollEnabled === true
                ) {
                  this.closeSelf();
                }
              }}
            >
              <View style={styles.content} {...this._panResponder.panHandlers}>
                <Text style={styles.headline}>{this.props.payload.title}</Text>
                <Image
                  source={{ uri: this.props.payload.image }}
                  style={styles.image}
                />

                {paragraph}
              </View>
              <EvilIcons name="chevron-down" style={styles.swipeDown} />
            </ScrollView>
          </Animated.View>
        </Animated.View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "#ffffff",
    height: WINDOW_HEIGHT,
    width: WINDOW_WIDTH,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  content: {
    paddingTop: 50,
    paddingBottom: 20
  },
  headline: {
    fontSize: 24,
    fontWeight: "bold",
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 20
  },
  sentence: {
    fontSize: 16,
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 20
  },
  image: {
    height: 150,
    width: WINDOW_WIDTH,
    marginBottom: 20
  },
  swipeDown: {
    flex: 1,
    fontWeight: "500",
    fontSize: 50,
    color: "#fff",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 0.5 },
    textShadowRadius: 5,
    position: "absolute",
    alignSelf: "center",
    top: 15
  }
});
