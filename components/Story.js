import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Button,
  Dimensions,
  ScrollView
} from "react-native";

const WINDOW_HEIGHT = Dimensions.get("window").height;
const WINDOW_WIDTH = Dimensions.get("window").width;

export default class ArticleModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      storyOrder: [],
      refresh: false
    };
  }

  componentWillMount() {
    this.getStoryOrder();
  }

  componentWillReceiveProps() {
    console.log("story refresh");
    console.log(this.props.refresh);
    if (this.state.refresh !== this.props.refresh) {
      this.getStoryOrder();
      this.setState({ refresh: this.props.refresh });
    }
  }

  getStoryOrder() {
    console.log("fetch");
    return fetch("https://coinstoryapp.com/getStoryOrder")
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.meta.code === 200) {
          let store = responseJson.data;
          this.setState({ storyOrder: store });
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  open(source) {
    this.props.openStory(source);
  }

  story() {
    // if(this.props.storyOrder !== undefined){
    return this.state.storyOrder.map((item, index) => {
      return (
        <TouchableOpacity
          onPress={() => this.open(item.name)}
          title="Open modal"
          key={index}
        >
          <Image source={{ uri: item.story }} style={styles.thumbnail} />
        </TouchableOpacity>
      );
    });
    // }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Stories</Text>
        <ScrollView
          horizontal={true}
          decelerationRate={0}
          snapToInterval={120}
          snapToAlignment={"center"}
          showsHorizontalScrollIndicator={false}
        >
          {this.story()}
          {/* <TouchableOpacity
                  onPress={() => this.open('cointelegraph')}
                  title="Open modal"
            >
                <Image source={{uri:'https://i.imgur.com/upaf58p.png'}} style={styles.thumbnail} />
            </TouchableOpacity>
            <TouchableOpacity
                  onPress={() => this.open('coindesk')}
                  title="Open modal"
            >
                <Image source={{uri:'https://i.imgur.com/vK7joob.png'}} style={styles.thumbnail} />
            </TouchableOpacity>        
            <TouchableOpacity
                  onPress={() => this.open('ethnews')}
                  title="Open modal"
            >
                <Image source={{uri:'https://i.imgur.com/hBw245W.png'}} style={styles.thumbnail} />
            </TouchableOpacity>            
            <TouchableOpacity
                  onPress={() => this.open('bitcoinist')}
                  title="Open modal"
            >
                <Image source={{uri:'https://i.imgur.com/vDN4EGB.png'}} style={styles.thumbnail} />
            </TouchableOpacity>             */}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    maxHeight: 195,
    flexGrow: 1,
    backgroundColor: "rgba(245, 245, 245, 1)",
    width: WINDOW_WIDTH,
    minHeight: 180
  },
  label: {
    color: "#222",
    fontWeight: "bold",
    paddingLeft: 10,
    paddingTop: 5
  },
  thumbnail: {
    height: 150,
    width: 100,
    borderRadius: 10,
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 10,
    opacity: 1
  }
});
