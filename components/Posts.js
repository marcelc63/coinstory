import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image, // we want to use an image
  ImageBackground,
  TouchableWithoutFeedback,
  Dimensions
} from "react-native";
import moment from "moment";
import Swiper from "react-native-swiper";
import TouchDouble from "./TouchDouble.js";

const WINDOW_HEIGHT = Dimensions.get("window").height;
const WINDOW_WIDTH = Dimensions.get("window").width;

export default class Posts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  article() {
    return this.props.newsFeedStore.map((item, index) => {
      let _swiper = {
        total: item.content.length - 1,
        index: 0
      };
      const _swiperScroll = () => {
        if (_swiper.index < _swiper.total) {
          _swiper.method.scrollBy(1);
        }
      };
      let author = item.source.charAt(0).toUpperCase() + item.source.slice(1);
      return (
        <View style={styles.post} key={index}>
          <Text style={styles.author}>{author}</Text>
          <Swiper
            ref={swiper => {
              _swiper.method = swiper;
            }}
            style={styles.swiper}
            showsButtons={false}
            showsPagination={true}
            activeDotColor={"rgba(0,0,0,.7)"}
            dotColor={"rgba(0,0,0,.2)"}
            loop={false}
            paginationStyle={styles.pagination}
            onIndexChanged={i => (_swiper.index = i)}
          >
            {item.content.map((x, i) => {
              if (x.type === "text") {
                return (
                  <TouchDouble
                    handlePress={() => _swiperScroll()}
                    key={i}
                    style={styles.slide}
                  >
                    <Text style={styles.text}>{x.summary}</Text>
                  </TouchDouble>
                );
              } else if (x.type === "image") {
                return (
                  <TouchDouble handlePress={() => _swiperScroll()} key={i}>
                    <ImageBackground
                      source={{ uri: x.image }}
                      style={styles.image}
                    >
                      {/* <Text style={styles.title}>{x.title}</Text>     */}
                    </ImageBackground>
                  </TouchDouble>
                );
              }
            })}
          </Swiper>
          <Text style={styles.caption}>
            <Text style={styles.captionAuthor}>{author}</Text> {item.title}
          </Text>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.openArticle({
                image: item.image,
                title: item.title,
                uniqid: item.uniqid
              });
            }}
          >
            <View>
              <Text style={styles.action}>Read full article</Text>
            </View>
          </TouchableWithoutFeedback>
          <Text style={styles.timestamp}>
            {moment
              .unix(item.timestring)
              .fromNow()
              .toUpperCase()}
          </Text>
        </View>
      );
    });
  }

  render() {
    return <View>{this.article()}</View>;
  }
}

const styles = StyleSheet.create({
  swiper: {
    height: WINDOW_WIDTH * 0.65 + 20
  },
  post: {
    marginBottom: 5
  },
  image: {
    height: WINDOW_WIDTH * 0.65,
    width: WINDOW_WIDTH
  },
  slide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(245, 245, 245, 1)",
    maxHeight: WINDOW_WIDTH * 0.65
  },
  text: {
    color: "#222",
    fontSize: 18,
    fontWeight: "400",
    paddingLeft: 10,
    paddingRight: 10
  },
  title: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  },
  author: {
    color: "#222",
    fontWeight: "bold",
    paddingLeft: 15,
    paddingBottom: 10,
    paddingTop: 10
  },
  pagination: {
    bottom: 0
  },
  caption: {
    fontSize: 14,
    color: "#222",
    marginTop: 5,
    marginBottom: 5,
    paddingLeft: 10,
    paddingRight: 10
  },
  captionAuthor: {
    fontSize: 14,
    fontWeight: "600"
  },
  action: {
    fontSize: 14,
    color: "rgb(153, 153, 153)",
    fontWeight: "400",
    marginBottom: 5,
    paddingLeft: 10,
    paddingRight: 10
  },
  timestamp: {
    fontSize: 10,
    color: "rgb(153, 153, 153)",
    paddingLeft: 10,
    paddingRight: 10
  }
});
