import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image, // we want to use an image
  ImageBackground,
  Button,
  ScrollView,
  Dimensions,
  Platform,
  RefreshControl,
  TouchableWithoutFeedback,
  AsyncStorage,
  AppState
} from "react-native";
import { EventRegister } from "react-native-event-listeners";
import { MonoText } from "./StyledText";

import StoryModal from "./StoryModal.js";
import ArticleModal from "./ArticleModal.js";
import Story from "./Story.js";
import Posts from "./Posts.js";
import PromptLang from "./PromptLang.js";

const WINDOW_HEIGHT = Dimensions.get("window").height;
const WINDOW_WIDTH = Dimensions.get("window").width;

export default class NewsFeed extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      storyVisible: false,
      articleVisible: false,
      newsFeedStore: [],
      articleStore: [],
      storyCache: [],
      isScrollBottom: false,
      page: 0,
      refreshing: false,
      refreshStory: false,
      appState: AppState.currentState,
      source: "",
      lang: "EN"
    };
  }

  componentWillMount() {
    this.getArticle(this.state.page);
    this._eventListener();
  }

  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  componentDidUpdate() {
    console.log("Update News Feed");
    this.state.isScrollBottom = false;
    this.getLang(lang => {
      if (lang !== this.state.lang) {
        this.setState({ lang: lang });
        this.getArticle(0, false);
      }
    });
  }

  getLang(callback) {
    AsyncStorage.getItem("lang", (e, res) => {
      callback(res);
    });
  }

  getArticle(page, option = false) {
    console.log("fetch");
    this.getLang(lang => {
      if (lang === null) {
        lang = "EN";
      }
      return fetch(
        "https://coinstoryapp.com/getArticle?page=" + page + "&lang=" + lang
      )
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.meta.code === 200) {
            let store =
              option === false
                ? this.state.newsFeedStore.concat(responseJson.data)
                : responseJson.data;
            this.setState({ newsFeedStore: store });
          }
        })
        .catch(error => {
          console.error(error);
        });
    });
  }

  _eventListener() {
    this.listener = EventRegister.addEventListener("scrollToTop", data => {
      this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true });
    });
    this.listener = EventRegister.addEventListener(
      "homeScreenDidFocus",
      data => {
        this.getArticle(0, true);
      }
    );
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.getArticle(0, true);
      this.setState({
        refreshing: false,
        refreshStory: !this.state.refreshStory,
        page: 0
      });
    }, 2000);
  }

  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      console.log("App has come to the foreground!");
      this.getArticle(0, true);
    }
    this.setState({ appState: nextAppState });
  };

  _promptLang() {
    console.log("prompt");
    this.getArticle(0, true);
  }

  openStory(source) {
    this.setState({ source: source }, () => {
      this.setState({ storyVisible: true });
    });
  }
  closeStory() {
    this.setState({ storyVisible: false });
  }

  openArticle(articleStore) {
    this.setState({ articleStore: articleStore }, () => {
      this.setState({ articleVisible: true });
    });
  }
  closeArticle() {
    this.setState({ articleVisible: false });
  }

  isCloseToBottom({ layoutMeasurement, contentOffset, contentSize }) {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  }

  storyModal() {
    if (this.state.storyVisible) {
      return (
        <StoryModal
          storyVisible={this.state.storyVisible}
          closeStory={this.closeStory.bind(this)}
          payload={this.state.source}
        />
      );
    }
    return null;
  }

  articleModal() {
    //Model
    let { articleVisible, articleStore } = this.state;

    if (this.state.articleVisible) {
      let payload = {
        image: articleStore.image,
        title: articleStore.title,
        uniqid: articleStore.uniqid
      };
      return (
        <ArticleModal
          articleVisible={articleVisible}
          closeArticle={this.closeArticle.bind(this)}
          payload={payload}
        />
      );
    }
    return null;
  }

  render() {
    return (
      <ScrollView
        ref="_scrollView"
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}
        onScroll={({ nativeEvent }) => {
          if (this.isCloseToBottom(nativeEvent)) {
            console.log("bottom");
            console.log(this.state.isScrollBottom);
            if (this.state.isScrollBottom === false) {
              this.state.isScrollBottom = true;
              this.state.page = this.state.page + 1;
              this.getArticle(this.state.page);
            }
          }
        }}
        scrollEventThrottle={100}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
          />
        }
      >
        <PromptLang handlePress={() => this._promptLang()} />
        {this.storyModal()}
        {this.articleModal()}
        <Story
          openStory={this.openStory.bind(this)}
          refresh={this.state.refreshStory}
        />
        <Posts
          newsFeedStore={this.state.newsFeedStore}
          openArticle={this.openArticle.bind(this)}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "rgba(255, 255, 255, 1)"
  }
});
