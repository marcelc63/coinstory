import React from "react";
import {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Image, // we want to use an image
  ImageBackground,
  PanResponder, // we want to bring in the PanResponder system
  Animated, // we wil be using animated value
  TouchableOpacity,
  TouchableWithoutFeedback,
  Modal,
  Button,
  Easing,
  Dimensions,
  ScrollView,
  AsyncStorage,
  StatusBar,
  Platform
} from "react-native";
import { LinearGradient } from "expo";
import { EvilIcons } from "@expo/vector-icons";
import moment from "moment";

import ArticleModal from "../components/ArticleModal.js";

const WINDOW_HEIGHT = Dimensions.get("window").height;
const WINDOW_WIDTH = Dimensions.get("window").width;
const isIphoneX =
  Platform.OS === "ios" && (WINDOW_HEIGHT === 812 || WINDOW_WIDTH === 812)
    ? true
    : false;

export default class StoryModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pan: new Animated.ValueXY(),
      scale: new Animated.Value(0),
      opacity: new Animated.Value(1),
      articleVisible: false,
      swipe: "none",
      slide: 0,
      showGradient: "none",
      intro: false,
      storyStore: [
        {
          image: "https://i.imgur.com/7ugibfn.png",
          title: "",
          uniqid: "",
          timestring: new Date().getTime()
        }
      ],
      progressBarWidth: WINDOW_WIDTH
    };
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderGrant: (e, gestureState) => {
        this.state.pan.setOffset({
          x: this.state.pan.x._value,
          y: this.state.pan.y._value
        });
        this.state.pan.setValue({ x: 0, y: 0 });
      },
      onPanResponderMove: (e, gestureState) => {
        if (
          gestureState.dy < -20 &&
          (this.state.swipe === "none" || this.state.swipe === "up")
        ) {
          this.state.swipe = "up";
          this.state.scale.setValue(1);
          this.openArticle();
        }
        if (
          gestureState.dy >= 0 &&
          (this.state.swipe === "none" || this.state.swipe === "down")
        ) {
          this.state.swipe = "down";
          Animated.event([
            null,
            {
              dy: this.state.pan.y
            }
          ])(e, gestureState);
        }
      },
      onPanResponderRelease: (e, gestureState) => {
        this.state.pan.flattenOffset();
        this.state.swipe = "none";

        if (gestureState.dy > 120) {
          Animated.timing(this.state.pan.y, {
            toValue: WINDOW_HEIGHT,
            duration: 100,
            easing: Easing.linear()
          }).start(() => {
            this.closeSelf();
          });
        } else {
          Animated.spring(this.state.pan, { toValue: 0, friction: 3 }).start(
            () => {}
          );
        }
      }
    });

    this.getStory();
    // this.intro()
  }

  intro() {
    if (this.state.intro === false) {
      this.setState({ intro: true });
      this.state.scale.setValue(0);
      Animated.timing(this.state.pan, {
        toValue: 1,
        duration: 100,
        easing: Easing.linear()
      }).start(() => {
        this.state.scale.setValue(1);
      });
    }
  }

  getLang(callback) {
    AsyncStorage.getItem("lang", (e, res) => {
      callback(res);
    });
  }

  getStory() {
    console.log("Fetch Story");
    this.getLang(lang => {
      if (lang === null) {
        lang = "EN";
      }
      let source = this.props.payload;
      return fetch(
        "https://coinstoryapp.com/getStory?source=" + source + "&lang=" + lang
      )
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.meta.code === 200) {
            //Resume Story Position
            AsyncStorage.getItem("StoryModal:" + source, (e, res) => {
              let store = responseJson.data;
              let slide = store.findIndex(x => x.uniqid === res);
              if (slide < 0) {
                slide = 0;
              }
              progressBarWidth = WINDOW_WIDTH / store.length * (slide + 1);
              this.setState({
                storyStore: store,
                slide: slide,
                progressBarWidth: progressBarWidth
              });
            });
          }
        })
        .catch(error => {
          console.error(error);
        });
    });
  }

  paginate = evt => {
    console.log(evt.nativeEvent);
    console.log(this.state.slide);
    if (evt.nativeEvent.locationX > WINDOW_WIDTH * 0.73) {
      if (this.state.slide < this.state.storyStore.length - 1) {
        let progressBarWidth =
          WINDOW_WIDTH / this.state.storyStore.length * (this.state.slide + 2);
        let slide = this.state.slide + 1;
        this.setState({ slide: slide, progressBarWidth: progressBarWidth });
      } else {
        this.closeSelf();
      }
    }
    if (evt.nativeEvent.locationX < WINDOW_WIDTH * 0.26) {
      if (this.state.slide > 0) {
        let progressBarWidth =
          WINDOW_WIDTH / this.state.storyStore.length * this.state.slide;
        let slide = this.state.slide - 1;
        this.setState({ slide: slide, progressBarWidth: progressBarWidth });
      }
    }
    this.setState({ showGradient: "none" });
  };
  paginateIn(evt) {
    if (evt.nativeEvent.locationX > WINDOW_WIDTH * 0.73) {
      this.setState({ showGradient: "right" });
    }
    if (evt.nativeEvent.locationX < WINDOW_WIDTH * 0.26) {
      this.setState({ showGradient: "left" });
    }
  }

  openArticle() {
    this.setState({ articleVisible: true, showGradient: "none" });
  }
  closeArticle() {
    this.setState({ articleVisible: false });
  }
  closeSelf() {
    //Save Story Position
    let payload = {
      source: this.state.storyStore[this.state.slide].source,
      uniqid: this.state.storyStore[this.state.slide].uniqid
    };
    AsyncStorage.removeItem("StoryModal:" + payload.source, () => {
      AsyncStorage.setItem("StoryModal:" + payload.source, payload.uniqid);
    });

    //Exit
    this.setState({ showGradient: "none", intro: false });
    this.props.closeStory();
  }

  _renderGradient() {
    if (this.state.showGradient === "left") {
      return (
        <LinearGradient
          colors={["rgba(0,0,0,0.3)", "transparent"]}
          start={[0, 1]}
          end={[1, 1]}
          style={{
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            height: 680,
            width: 50
          }}
        />
      );
    } else if (this.state.showGradient === "right") {
      return (
        <LinearGradient
          colors={["rgba(0,0,0,0.3)", "transparent"]}
          start={[1, 1]}
          end={[0, 1]}
          style={{
            position: "absolute",
            right: 0,
            top: 0,
            height: 680,
            width: 50
          }}
        />
      );
    } else {
      return null;
    }
  }

  articleModal() {
    //Model
    let { articleVisible, storyStore, slide } = this.state;
    let payload = {
      title: storyStore[slide].title,
      image: storyStore[slide].image,
      uniqid: storyStore[slide].uniqid
    };

    if (this.state.articleVisible) {
      return (
        <ArticleModal
          articleVisible={articleVisible}
          closeArticle={this.closeArticle.bind(this)}
          payload={payload}
        />
      );
    }
    return null;
  }

  render() {
    let { pan, articleVisible, storyStore, slide } = this.state;
    let [translateX, translateY] = [pan.x, pan.y];
    let rotate = "0deg";
    let scale = pan.y.interpolate({
      inputRange: [0, WINDOW_HEIGHT],
      outputRange: [1, 0.9]
    });
    let imageStyle = {
      transform: [{ translateX }, { translateY }, { rotate }, { scale }]
    };

    let backgroundOpacity = pan.y.interpolate({
      inputRange: [0, WINDOW_HEIGHT],
      outputRange: ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]
    });
    let backgroundModal = {
      backgroundColor: backgroundOpacity
    };

    let progressBar = {
      backgroundColor: "rgba(255,255,255,0.8)",
      height: 5,
      width: this.state.progressBarWidth,
      position: "absolute",
      top: 0,
      left: 0
    };
    let progressBariPhoneX = {
      backgroundColor: "rgba(255,255,255,0.8)",
      height: 2,
      width: this.state.progressBarWidth / 10,
      position: "absolute",
      top: 25,
      left: 15
    };

    //Model
    let payload = {
      title: storyStore[slide].title,
      image: storyStore[slide].image,
      timestring: moment.unix(storyStore[slide].timestring).fromNow(),
      source: storyStore[slide].source,
      uniqid: storyStore[slide].uniqid
    };

    if (payload.source !== undefined) {
      payload.source =
        payload.source.charAt(0).toUpperCase() + payload.source.slice(1);
    }

    return (
      <Modal
        visible={this.props.storyVisible}
        transparent={true}
        animationType={"fade"}
        onRequestClose={() => {}}
      >
        <StatusBar hidden={true} />
        <Animated.View style={backgroundModal}>
          {this.articleModal()}

          <Animated.View style={imageStyle} {...this._panResponder.panHandlers}>
            <TouchableWithoutFeedback
              onPressIn={evt => this.paginateIn(evt)}
              onPressOut={evt => this.paginate(evt)}
              style={styles.touchArea}
            >
              <View style={styles.image}>
                <ImageBackground
                  source={{ uri: payload.image }}
                  style={styles.image}
                >
                  <View style={isIphoneX ? progressBariPhoneX : progressBar} />
                  <View style={styles.header}>
                    <Text style={styles.source}>{payload.source}</Text>
                    <Text style={styles.timestring}>{payload.timestring}</Text>
                  </View>
                  <Text style={styles.title}>{payload.title}</Text>
                  <View style={styles.swipeUp}>
                    <EvilIcons name="chevron-up" style={styles.swipeUpIcon} />
                    <Text style={styles.swipeUpText}>Read Full Article</Text>
                  </View>

                  {this._renderGradient()}
                </ImageBackground>
              </View>
            </TouchableWithoutFeedback>
          </Animated.View>
        </Animated.View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    // backgroundColor: 'rgba(255,255,255,0.7)',
    fontSize: 35,
    fontWeight: "bold",
    position: "absolute",
    padding: 5,
    bottom: WINDOW_HEIGHT * 0.2,
    width: WINDOW_WIDTH,
    color: "#fff",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10
  },
  image: {
    height: WINDOW_HEIGHT,
    width: WINDOW_WIDTH,
    backgroundColor: "#222"
  },
  header: {
    marginTop: 35,
    paddingLeft: 15
  },
  source: {
    fontWeight: "600",
    paddingRight: 200,
    fontSize: 14,
    color: "#fff"
  },
  timestring: {
    fontWeight: "300",
    fontSize: 14,
    color: "#fff"
  },
  swipeUp: {
    flex: 1,
    position: "absolute",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    bottom: 25
  },
  swipeUpIcon: {
    color: "#fff",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -0.5, height: 0.5 },
    textShadowRadius: 5,
    fontSize: 50,
    marginBottom: -15,
    paddingBottom: 0
  },
  swipeUpText: {
    fontWeight: "500",
    color: "#fff",
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -0.5, height: 0.5 },
    textShadowRadius: 5
  },
  touchArea: {
    height: WINDOW_HEIGHT,
    width: WINDOW_WIDTH
  }
});
