import { Notifications, AppLoading } from "expo";
import React from "react";
import { AsyncStorage } from "react-native";
import { StackNavigator } from "react-navigation";

import MainTabNavigator from "./MainTabNavigator";
import registerForPushNotificationsAsync from "../api/registerForPushNotificationsAsync";
import OnboardingScreen from "../screens/OnboardingScreen.js";
import DetailsScreen from "../screens/DetailsScreen.js";

const RootStackNavigator = StackNavigator(
  {
    Main: {
      screen: MainTabNavigator
    },
    Details: {
      screen: DetailsScreen
    }
  },
  {
    navigationOptions: () => ({
      headerTitleStyle: {
        fontWeight: "bold"
      }
    }),
    initialRouteName: "Main"
  }
);

export default class RootNavigator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOnboarded: true
    };
  }

  componentDidMount() {
    this._notificationSubscription = this._registerForPushNotifications();
    AsyncStorage.getItem("onboard", (e, res) => {
      console.log(res);
      if (res === "onboarded") {
        this.setState({ isOnboarded: false });
      }
    });
  }

  componentWillUnmount() {
    this._notificationSubscription && this._notificationSubscription.remove();
  }

  _onboard() {
    console.log("Onboarded");
    this.setState({ isOnboarded: false });
  }

  render() {
    if (this.state.isOnboarded) {
      return <OnboardingScreen screenProps={() => this._onboard()} />;
    } else {
      return <RootStackNavigator />;
    }
  }

  _registerForPushNotifications() {
    // Send our push token over to our backend so we can receive notifications
    // You can comment the following line out if you want to stop receiving
    // a notification every time you open the app. Check out the source
    // for this function in api/registerForPushNotificationsAsync.js
    registerForPushNotificationsAsync();

    // Watch for incoming notifications
    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    );
  }

  _handleNotification = ({ origin, data }) => {
    console.log(
      `Push notification ${origin} with data: ${JSON.stringify(data)}`
    );
  };
}
