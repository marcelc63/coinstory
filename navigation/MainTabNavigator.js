import React from "react";
import { Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import {
  TabNavigator,
  TabBarBottom,
  NavigationActions
} from "react-navigation";
import { EventRegister } from "react-native-event-listeners";

import Colors from "../constants/Colors";

import HomeScreen from "../screens/HomeScreen";
import AnalyticsScreen from "../screens/AnalyticsScreen";
import SettingsScreen from "../screens/SettingsScreen";

export default TabNavigator(
  {
    Home: {
      screen: HomeScreen
    },
    Analytics: {
      screen: AnalyticsScreen
    },
    Settings: {
      screen: SettingsScreen
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case "Home":
            iconName = Platform.OS === "ios" ? `md-home` : "md-home";
            break;
          case "Analytics":
            iconName = Platform.OS === "ios" ? `ios-stats` : "md-stats";
            break;
          case "Settings":
            iconName = Platform.OS === "ios" ? `ios-settings` : "md-settings";
        }
        return (
          <Ionicons
            name={iconName}
            size={28}
            style={{ marginBottom: -3 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        );
      }
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: "bottom",
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions: {
      showLabel: false
    },
    tabBarComponent: ({ jumpToIndex, ...props }) => (
      <TabBarBottom
        {...props}
        jumpToIndex={index => {
          if (props.navigation.state.index === index) {
            EventRegister.emit("scrollToTop");
            jumpToIndex(index);
          } else {
            jumpToIndex(index);
          }
        }}
      />
    )
  }
);
